#!/bin/bash
MAX_BACKUPS=10
BACKUP_NAME=$(date +\%Y.\%m.\%d.\%H\%M\%S)

echo "=> Backup started"
#if mongodump --out /mongo_backup/${BACKUP_NAME} --host db --port 27017   ;then
if mongodump -uadmin -ppass -hdb --out /mongo_backup/${BACKUP_NAME}  ;then
    echo "   Backup succeeded___"`date` >> /mongo_backup/log.log
else
    echo "   Backup failed   ___"`date` >> /mongo_backup/log.log
    rm -rf /mongo_backup/${BACKUP_NAME}
fi

if [ -n "${MAX_BACKUPS}" ]; then
    while [ $(ls /mongo_backup -N1 | wc -l) -gt ${MAX_BACKUPS} ];
    do
        BACKUP_TO_BE_DELETED=$(ls /mongo_backup -N1 | sort | head -n 1)
        echo "   Deleting backup ${BACKUP_TO_BE_DELETED}"`date` >> /mongo_backup/log.log
        rm -rf /mongo_backup/${BACKUP_TO_BE_DELETED}
    done
fi
echo "=> Backup done" 
