FROM mongo:4.2
LABEL maintainer "victor efrain perez <victor3frain@gmail.com>" 

USER root
WORKDIR /mongoScript

RUN mkdir /mongo_backup
COPY  backup.sh /mongoScript
COPY  restore.sh /mongoScript

RUN chmod 0644 /mongoScript/backup.sh
RUN chmod 0644 /mongoScript/restore.sh


RUN apt update -y && \ 
    apt upgrade -y && \
    apt install cron -y && \
    apt install nano -y &&  \
    apt install -y vim

RUN crontab -l | { cat; echo "*/2 * * * * bash /script_backup/backup"; } | crontab -

#CMD cron
