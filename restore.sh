#!/bin/bash
echo "=> Restore database from $1"
if mongorestore --host db --port 27017  $1; then
    echo "   Restore succeeded"
else
    echo "   Restore failed"
fi
echo "=> Done"
